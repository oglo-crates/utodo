# UTodo

UTodo is a universal TODO list format, it uses TOML, but it uses TOML in a specific way.

```toml
# The version of UTodo that this list was generated with in mind.
utodo = [2, 0, 1] # This would be UTodo 2.0.1!

# Newly added tasks with no progress on them.
[pending]
items = [
  "Make a sandwich."
]

# Tasks with progress on them, but that are not finished.
[progress]
items = [
  "Make a cup of coffee.",
]

# Tasks that are finished.
[finished]
items = [
  "Brush teeth.",
]
```

There is also a standard for project integration:

Lists are stored in: `$PROJECT_DIR/.utodo/lists/$LISTNAME` (Note: there is no `.toml` on the end.)
